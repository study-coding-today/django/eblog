from django.db import models


class BaseModel(models.Model):
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        # 아래 abstract 를 True로 설정하여 BaseModel 를 추상클래스로 선언한 것임
        # 추상클래스로 선언하여 db에 table 를 만들지 않게 된다.
        abstract = True
