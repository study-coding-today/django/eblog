from django.shortcuts import render
from .forms import RegisterForm


def register(request):
    # HTML 에 form 태그에 전송방식이 POST 일때 확인
    if request.method == 'POST':
        user_form = RegisterForm(request.POST)
        if user_form.is_valid():
            # commit 을 False 로 사용하여 db에 저장이 안되고 메모리에 object 로 생성된다.
            user = user_form.save(commit=False)
            user.set_password(user_form.cleaned_data['password'])
            user.save()
            return render(request, 'registration/login.html', {'user': user})
    else:
        user_form = RegisterForm()

    return render(request, 'registration/register.html', {'user_form': user_form})
