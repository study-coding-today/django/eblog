from django import forms
from .models import User


class RegisterForm(forms.ModelForm):
    # 회원가입 폼
    # 장고에서는 HTML 입력요서를 widget 이라고 함
    # 아래 항목은 HTML 의 input 태그에서 입력되는 멤버변수로 class 들이다.
    password = forms.CharField(label='password', widget=forms.PasswordInput)
    confirm_password = forms.CharField(label='confirm password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'gender', 'email']

        # 위 confirm_password 의 유효성검사를 위한 메서드로 Clean_멤버변수(class)로 이름 짓는다.
    def clean_confirm_password(self):
        cd = self.cleaned_data
        if cd['password'] != cd['confirm_password']:
            raise forms.ValidationError('비밀번호가 일치하지 않습니다.')

        return cd['confirm_password']